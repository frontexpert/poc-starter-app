import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {AppData} from '../../providers/app-data';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  htmlContent = '';

  constructor(public navCtrl: NavController, private appData: AppData) {
    appData.load().then(result => {
      this.htmlContent = result;
    })
  }

}
