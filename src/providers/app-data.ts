import { Injectable } from '@angular/core';

import { Http } from '@angular/http';

@Injectable()
export class AppData {
  data: any;

  constructor(public http: Http) {}

  load() {
    if (this.data) {
      // already loaded data
      return Promise.resolve(this.data);
    }

    // don't have the data yet
    return new Promise(resolve => {
      // We're using Angular Http provider to request the data,
      // then on the response it'll map the JSON data to a parsed JS object.
      // Next we process the data and resolve the promise with the new data.
      this.http.get('http://upwork-test.mayteio.xyz/').subscribe(res => {
        this.data = res.text();
        resolve(this.data);
      });
    });
  }
}